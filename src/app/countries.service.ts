import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Countries } from './countries';
import { Observable } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class CountriesService {


  constructor(private http:HttpClient) {     
  }

  getContries():Observable<Countries[]>{
    return this.http.get<Countries[]>("https://restcountries.com/v3.1/all")
  }
}
