import { Component, OnInit } from '@angular/core';
import { CountriesService } from '../countries.service';
import { Countries } from '../countries';
import { Router } from '@angular/router';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-countries-list',
  templateUrl: './countries-list.component.html',
  styleUrls: ['./countries-list.component.css'],
})
export class CountriesListComponent implements OnInit {
  reqData: Countries[] = [];
   searchedCountry = ""

  constructor(public countries: CountriesService, private router: Router) {}
  

  ngOnInit(): void {
    this.countries.getContries().subscribe((result) => (this.reqData = result));
  }

  onSelect(country: any) {
    //this.router.navigate(['/departments', department.id]);
    this.router.navigate(['/countries', country.name.commons]);
    console.log(this.searchedCountry)
  }
  onChange(searchedCountry: string){
    this.countries.getContries().subscribe((result) => (this.reqData = result.filter((eachCountry)=>{
      return eachCountry.name.common.toLowerCase().includes(searchedCountry.toLowerCase())
    })));
  }
}
