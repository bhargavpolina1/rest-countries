import { Component, OnInit } from '@angular/core';
import { CountriesService } from '../countries.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Countries } from '../countries';

@Component({
  selector: 'app-country-detail',
  templateUrl: './country-detail.component.html',
  styleUrls: ['./country-detail.component.css']
})
export class CountryDetailComponent implements OnInit {

  

  reqData: Countries[] = [];
  isDataRender = (this.reqData.length === 0)
  

  constructor(public countries: CountriesService, private router: Router, private route: ActivatedRoute) {}

  ngOnInit(): void {
    const nameinRoute = this.route.snapshot.paramMap.get('name')
    console.log(nameinRoute)
    this.countries.getContries().subscribe((result) => (this.reqData = result.filter((country) => {


      return country.name.common === nameinRoute
    })));

    
  }


}
